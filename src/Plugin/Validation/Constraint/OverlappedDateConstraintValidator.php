<?php

namespace Drupal\cool_calendar_extras\Plugin\Validation\Constraint;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class to validate OverlappedDate constraint in OverlappedDateConstraint.php.
 */
class OverlappedDateConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The entity type manager, used to fetch entity link templates.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The string translation service.
   *
   * @var Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentRouteMatch $currentRouteMatch, EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory, TranslationInterface $stringTranslation) {
    $this->currentRouteMatch = $currentRouteMatch;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * This function will execute every time that the user save a entity/node.
   */
  public function validate($value, Constraint $constraint) {

    // $value --> in this variable we have the node.
    // $constraint --> the class information OverlappedDateConstraint.php.
    // Check if it is a node.
    if ($value->getEntityTypeId() != 'node') {
      // Is not a node, discontinue the validation.
      return;
    }

    // Get the configuration that is set into administration section.
    $config = $this->configFactory->get('cool_calendar_extras.settings');

    // Get the module configuration.
    if (is_null($config)) {
      // No module configuration exist, discontinue the validation.
      return;
    }

    // Generate the fields names that they should have.
    $name_module = 'cool_calendar_extras';

    $config_smartdate = $name_module . '_' . $value->bundle() . '_constraint' . '_smartdate';
    $config_taxonomy = $name_module . '_' . $value->bundle() . '_constraint' . '_taxonomy';

    // Check if some restriction is configured for this type of content.
    if (is_null($config->get($config_smartdate)) || $config->get($config_smartdate) == 'none') {
      // No restriction present, discontinue the validation.
      return;
    }

    // Get an existing schedule list/array between dates indicated.
    $nodes_overlapped = $this->getOverlappedEvents($value, $config->get($config_smartdate), $config->get($config_taxonomy));

    // If we have got at least one overlapped element...
    // If number of nodes found is > 0 --> return TRUE (overlapped elements).
    // If number of nodes found is 0 --> return FALSE (no overlapped elements).
    if (count($nodes_overlapped) > 0) {

      // Generate a text with all elements that exist overlapping/conflict.
      $nodes_overlapped_str = "<br>";

      foreach ($nodes_overlapped as $node) {
        $nodes_overlapped_str .= "- <a href='" . $node->toUrl()->toString() . "' target='_blank' >" . $node->get('title')->getString() . "</a><br>";
      }

      // Concat the error message with the overlapped elements list.
      $violation_str = $this->t('The configuration that you have set overlaps with other elements:') . $nodes_overlapped_str;

      // There are overlapping dates, we inform about this fact and we don't
      // allow to save until the user modify the current node values.
      $this->context->addViolation($violation_str);

    }

  }

  /**
   * Get entities/nodes that have the dates overlapped with current node.
   */
  private function getOverlappedEvents($node, $smart_date_field_name, $taxonomy_field_name) {

    // Get the entity type name.
    $node_entity_type = $node->getEntityTypeId();

    // Get the bundle.
    $node_bundle = $node->bundle();

    // Get the smart_date field value.
    $smart_date_field_value = $node->get($smart_date_field_name)->getValue();

    // Get the smart_date field configuration, the limit of months
    // that will be able to generate the occurrences.
    $smart_date_field_config = $this->entityTypeManager->getStorage('field_config')->load($node_entity_type . '.' . $node_bundle . '.' . $smart_date_field_name);
    // If the value doesn't exist or is NULL, by default set it to 12 months.
    $month_limit = $smart_date_field_config->getThirdPartySetting('smart_date_recur', 'month_limit', 12);

    // Use next function (from smart_date_recur.module) that will return an
    // array with all occurrences of the field that are saved.
    $recurrences = smart_date_recur_generate_rows($smart_date_field_value, $node_entity_type, $node_bundle, $smart_date_field_name, $month_limit);

    // Get all nodes queue that belongs to same entity as node for validiting
    // and also they have the same taxonomy term.
    $query = $this->entityTypeManager->getStorage('node')->getQuery();

    // TRUE = that only take into account visible results for user,
    // FALSE = all results.
    $query->accessCheck(FALSE);
    // Only which are registered.
    $query->condition('status', 1);
    // That the type of content be a specific.
    $query->condition('type', $node_bundle);

    // Check if taxonomy restriction config exists for this type of content.
    if (!is_null($taxonomy_field_name) && $taxonomy_field_name != 'none') {

      // Get the taxonomy term identifier that user has set in the node.
      $node_term_id = $node->get($taxonomy_field_name)->getString();

      // Exists a taxonomy restriction, so, we add a  condition into the query.
      // Only nodes that have a vocabulary field in them has specific term id.
      $query->condition($taxonomy_field_name, $node_term_id);

    }

    // Node id for modifying an existing node.
    if (!is_null($node->id())) {
      $query->condition('nid', $node->id(), '<>');
    }

    // Array to save all node-identifiers for an overlapping conflict.
    $nodes_id = [];

    // For each recurrence a query is generated from $query as base
    // ($query has all common conditions for all queries that we will execute).
    foreach ($recurrences as $recurrence) {

      // Clone the base query to make a query for this iteration with specific
      // values.
      $sub_query = clone $query;

      // Get the start and end timestamp.
      $date_start = $recurrence['value'];
      $date_end = $recurrence['end_value'];

      // Create a AND condition.
      $andGroup = $sub_query->andConditionGroup();

      // Add in the AND condition the following conditions:
      // .end_value > $data_start AND .value < $date_end.
      $andGroup->condition($smart_date_field_name . '.end_value', $date_start, '>');
      $andGroup->condition($smart_date_field_name . '.value', $date_end, '<');

      // Add in the AND condition into the recurrence query.
      $sub_query->condition($andGroup);

      // Execute the query for the current occurrence and we add the results
      // (overlapped nodes) into the array.
      $nodes_id = array_merge($nodes_id, $sub_query->execute());

    }

    // Load the objects (nodes) from the ids based on previous query.
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nodes_id);

    return $nodes;

  }

}
